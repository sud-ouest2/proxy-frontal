#!/bin/bash
# Script Licence: GNU/GPL v3
#   2017 - Eric Seigne <eric.seigne@abul.org>
# automatic renew let's encrypt certificates

function ldebug()
{
    DEBUG=0
    if [ ${DEBUG} == 1 ]; then
	echo $1
    fi
}

MYIPS=`ip -f inet -o addr | awk '!/^[0-9]*: ?lo|link\/ether/ {gsub("/", " "); print $4}'`
ldebug "-> mon ip ${MYIP}"

rm -f /etc/dehydrated/domains.txt

FIC="/etc/apache2/reference_soo"
if [ -f  "/etc/apache2/reference_soo.new" ]; then
    ldebug "on utilise /etc/apache2/reference_soo.new"
    FIC="/etc/apache2/reference_soo.new"
fi
ldebug "on supprime ${FIC}.ok"
rm -f ${FIC}.ok

for domaine in `cat ${FIC}`
do
    ldebug "on bosse sur ${domaine} .."
    #test si le domaine existe ...
    TESTDOMAINE=`getent hosts ${domaine} | head -n1 | awk '{print $1}'`
    for myip in ${MYIPS}
    do
	if [ "${TESTDOMAINE}" = "${myip}" ]; then
	    ldebug "${TESTDOMAINE}  est egal a ${myip} on ajoute ${domaine} a ${FIC}.ok..."
	    echo ${domaine} >> ${FIC}.ok
	    break
	fi
    done
done

cat /etc/dehydrated/domains.txt_origine ${FIC}.ok > /etc/dehydrated/domains.txt

dehydrated -c --hook /home/ericsadmin/proxy-frontal/dehydrated/hook.sh

#le certificat mail.sud-ouest2.org est un cas particulier
if [ ! -d /etc/apache2/keys/mail.sud-ouest2.org/ ]; then
    mkdir /etc/apache2/keys/mail.sud-ouest2.org/
fi

for fic in cert.pem privkey.pem chain.pem
do
    scp robot-ssl@mail:/var/lib/dehydrated/certs/mail.sud-ouest2.org/${fic} /etc/apache2/keys/mail.sud-ouest2.org/
done

#probleme: si un adherent ajoute une demande d'hebergement de liste et qu'il ne fait pas l'ajout de son dns pour faire
#pointer chez nous ... on a un diff qui dit qu'on devrait avoir une liste hebergee mais en fait non donc le restart
#apache n'est pas necessaire ...

if [ -f /tmp/apache_restart_dehydrated ]; then
    service apache2 restart
    rm -f /tmp/apache_restart_dehydrated
fi


