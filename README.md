# Configuration et scripts pour le/les serveurs frontal web (proxy)

L'infrastructure de sud-ouest2.org est prévue pour avoir un ou plusieurs serveurs web frontaux (proxy webs) "cachant" les applications web

Le principe est le suivant:
* La gestion administrative est gérée par Dolibarr
* Le système central est géré par Modoboa
* Un ou plusieurs serveurs web frontaux

## Installation OS

* OS de base : Debian GNU/Linux
* Paquets logiciels installés : voir le contenu du fichier dpkg.get_selections

## Interface de gestion ? webmin ...

L'idée de départ était de proposer des outils "faciles à reprendre" lorsque ma mission d'installation initiale se terminera ... néanmoins je ne suis pas convaincu par webmin dont je trouve l'interface encore très très loin de l'ergonomie d'un bon pfsense ... résultat webmin n'est pas / plus utilisé !

## Installation OS

Tous les scripts personnalisés sont dans /usr/local/bin :

* soo2-update_certs.sh : mise à jour automatique des certificats ssl
* soo2-update_proxy.sh : mise à jour automatique des virtualhosts à activer en fonction des choix d'hébergements qui ont été faits sur dolibarr

un adhérent se connecte sur l'interface de gestion administrative (dolibarr) et dédide de nous confier la gestion du sous domaine listes.sondomaineperso.extension ou de son domaine web ... et quelques minutes plus tard tout est prêt sur ce serveur ... (en fonction du cron que vous avez configurés)

