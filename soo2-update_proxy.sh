#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
#comme on est proxy il faut qu'on sache quels sont les domaines virtuels qu'on heberge ...
#requete sur dolibarr pour avoir la liste ... https://sud-ouest2.org/dolibarr/public/soo/get_liste_virtualhosts.php
FIC=`tempfile`

LADATE=`date "+%Y%m%d %H:%M"`
echo "------------- ${LADATE}"

function ldebug()
{
    DEBUG=0
    if [ ${DEBUG} == 1 ]; then
	echo $1
    fi
}

wget -q https://sud-ouest2.org/dolibarr/public/soo/get_liste_virtualhosts.php -O ${FIC}
if [ $? != 0 ]; then
    ldebug "Erreur wget ...."
    exit
fi

#detection d'erreur http ... normalement on a que des lignes qui commencent par "listes."
TESTLISTES=`grep "^listes." ${FIC}`
if [ -z "${TESTLISTES}" ]; then
    exit
fi

#pour ne pas tout refaire si pas de nouveautes
diff ${FIC} /etc/apache2/reference_soo
if [ $? == 0 ]; then
    #aucune modif -> exit
    rm ${FIC}
    exit
fi

#avant tout on fait une mise a jour des certificats (pour une nouvelle liste par exemple) tant que apache tourne
cp ${FIC} /etc/apache2/reference_soo.new
/usr/local/bin/soo2-update_certs.sh

#on commence par desactiver toutes les listes pour gerer le cas des suppression des comptes ou des domaines
cd /etc/apache2/sites-enabled
for domaine in listes.*
do
    ldebug "on bosse sur ${domaine} .."
    dom=`echo ${domaine} | sed s/".conf$"/""/`
    #test si le domaine existe ...
    TESTDOMAINE=`getent hosts ${dom} | head -n1`
    if [ -n "${TESTDOMAINE}" ]; then
	/usr/sbin/a2dissite ${dom}
    fi
done

MYIPS=`ip -f inet -o addr | awk '!/^[0-9]*: ?lo|link\/ether/ {gsub("/", " "); print $4}'`
ldebug "-> mon ip ${MYIP}"

for liste in `cat ${FIC}`
do
    ldebug "on bosse(2) sur ${liste} .."
    domaine=`echo ${liste} | sed s/"^listes."/""/`
    #test si le domaine existe ...
    TESTDOMAINE=`getent hosts ${domaine} | head -n1`
    #test si le sous domaine existe ... car certains ont le sous domaine mais pas le domaine
    TESTSOUSDOMAINE=`getent hosts ${liste} | head -n1`
    if [ -n "${TESTDOMAINE}" -o -n "${TESTSOUSDOMAINE}" ]; then
	ldebug "  on active ${liste}"
	autoconfig=`echo ${liste} | sed s/"listes"/"autoconfig"/`
	cat /etc/apache2/sites-available/virtualhost_sympa.maquette /etc/apache2/sites-available/virtualhost_sympa.maquette_ssl | sed s/"DOMAINE"/"${domaine}"/ | sed s/"MAQUETTE"/"${liste}"/ | sed s/"AUTOCONFIG"/"${autoconfig}"/ > /etc/apache2/sites-available/${liste}.conf
	#bug du certificat non present ...
	if [ -f /var/lib/dehydrated/certs/${liste}/cert.pem ]; then
	    /usr/sbin/a2ensite ${liste}
	else
	    echo "  [error] le certificat pour ${liste} n'est pas encore disponible, ce domaine n'est pas activé"
	fi
    else
	ldebug "  on supprime ${liste} car le getent sur le domaine ${domaine} n'a rien retourné"
	sed -i -e "/${domaine}/d" ${FIC}
    fi
done

cp ${FIC} /etc/apache2/reference_soo
rm ${FIC}
#rm /etc/apache2/reference_soo.new

service apache2 restart

